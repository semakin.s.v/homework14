#include <iostream>
#include <string>

int main()
{
    std::string str = "Hello, my Friend!"; 

    std::cout << str << std::endl;

    std::cout << str.length() << "\n";
    std::cout << str[0] << "\n";
    std::cout << str[str.length()-1] << "\n";
}


